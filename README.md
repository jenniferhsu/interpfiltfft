Jennifer Hsu (jsh008@ucsd.edu)
Music 267 - Assignment 1
Date: October 29, 2013

Pd external that interpolates between two user supplied filters (in the frequency domain).  Takes in a signal (in the time domain) takes the FFT of that signal and filters the input signal with the interpolated filters.  Interpolation between the two filters can be specified with a float value (control rate) or a signal (for audio rate control).

Developed for Pd-0.45-2.

Compile the files:

1. Please modify your makefile inside the \{directory_with_pd\}/Contents/Resources/doc/6.externs to look like mine (just adding my c file - interpFiltFFT~.c to the list)
2. Copy or  move randperm.c and interpFiltFFT~.c into the \{directory_with_pd\}/Contents/Resources/doc/6.externs directory
3. Open up a terminal and type "make pd_darwin"
4. Copy or move randperm.pd_darwin and interpFiltFFT~.pd_darwin into \{directory_with_pd\}/Contents/Resources/extra directory


For using the external, please see the included example patch.
