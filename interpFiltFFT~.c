// =========================================================================
// name: interpFiltFFT~.c
// desc: extern for pd
//          'interpFiltFFT n'
//             inputs:
//	    	 takes in a float on the left inlet (equal to the n in the
//                  creation argument and morphs from one filter to the other
//                  by the amount specifed by n (0 <= n <= 1)
//               also takes in a signal (in the time domain) on the left inlet
//                  - this signal will be filtered by the next two inlets
//               takes in a magnitude spectrum (as a signal) on the next inlet
//                  - will be used as the first filter
//               the next inlet takes in a magnitude spectrum (as a signal)
//                  - will be used as the second filter
//               the next inlet takes in a signal (in the time domain)
//                  - this signal is used to modulate n
//               the rightmost inlet takes in a number to change n
//             outputs:
//               the left output: filtered signal
//               the right output: interpolated filter magnitude spectrum
//	  FFT functions are from chuck_fft.h and chuck_fft.c by Ge Wang and Perry Cook
//               based on code from the San Diego CARL package
//
// author: jennifer hsu
// date: fall 2013
// ==========================================================================

#include "m_pd.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
//#include <stdlib.h>
//#include <time.h>


// struct type to hold all variables that have to do with the interpFiltFFT~ object
typedef struct _interpFiltFFT
{
    t_object x_obj;
    t_float step;
    int useTimeSig;
    t_float * window;
} t_interpFiltFFT;

// interpFiltFFT~ class that will be created in 'setup'
// and used in 'new' to create new instances
t_class *interpFiltFFT_class;

// FFT typedef and constants
// complex type
typedef struct { float re ; float im ; } complex;
// complex absolute value
#define cmp_abs(x) ( sqrt( (x).re * (x).re + (x).im * (x).im ) )
// phase
#define cmp_phase(x) ( atan2((double)(x).im, (double)(x).re) )
#define FFT_FORWARD 1
#define FFT_INVERSE 0


/* function prototypes */
// Pd extern functions
static t_int *interpFiltFFT_perform(t_int *w);
static void interpFiltFFT_dsp(t_interpFiltFFT *interpFiltFFT_obj, t_signal **sp);
void messageUsage(void);
void interpFiltFFT_set(t_interpFiltFFT *interpFiltFFT_obj, t_symbol *selector, int argc, t_atom *argv);
void interpFiltFFT_step(t_interpFiltFFT *interpFiltFFT_obj, t_floatarg f);
static void *interpFiltFFT_new(void);
void interpFiltFFT_tilde_setup(void);
// FFT functions
void make_window( float * window, unsigned long length );
void apply_window( float * data, float * window, unsigned long length );
void rfft( float * x, long N, unsigned int forward );
void complexfft( float * x, long NC, unsigned int forward );
void bit_reverse( float * x, long N );


// perform function (audio callback)
static t_int *interpFiltFFT_perform(t_int *w)
{
    // extract data
    t_interpFiltFFT *interpFiltFFT_obj = (t_interpFiltFFT *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    t_float *inFilt1 = (t_float *)(w[3]);
    t_float *inFilt2 = (t_float *)(w[4]);
    t_float *inSigTime = (t_float *)(w[5]);
    t_float *out = (t_float *)(w[6]);
    t_float *outFilt = (t_float *)(w[7]);
    int totalSamples = (int)(w[8]);
    
    // process samples
    
    // no windows for now
    //apply_window(in, interpFiltFFT_obj->window, totalSamples);
    
    // fft of input signal
    rfft( in, totalSamples/2, FFT_FORWARD );

    
    // process input with interpolated filters
    int sample;
    
    for(sample = 0; sample < totalSamples; sample++)
    {
        // interpolate filters to correct value
        if(interpFiltFFT_obj->useTimeSig)
            *(outFilt+sample)= *(inFilt1+sample) + interpFiltFFT_obj->step*(*(inFilt2+sample) - *(inFilt1+sample));
        else
            *(outFilt+sample) = *(inFilt1+sample) + fabs(*(inSigTime+sample))*(*(inFilt2+sample) - *(inFilt1+sample));
        
        // multiply interpolated filter spectrum with input signal spectrum
        *(in+sample) = *(in+sample) * (*(outFilt+sample));
        
    }
    
    
    // inverse fft the spectrum of the input
    rfft( in, totalSamples/2, FFT_INVERSE );
    
    // output interpolated spectrum and filtered signal
    for( sample = 0; sample < totalSamples; sample++ )
    {
        *(out+sample) = *(in+sample);
    }
    
    
    // return pointer to next set of data
    return (w+9);
}

// called to start dsp, register perform function
static void interpFiltFFT_dsp(t_interpFiltFFT *interpFiltFFT_obj, t_signal **sp)
{
    // add the perform function
    dsp_add(interpFiltFFT_perform, 8, interpFiltFFT_obj, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[3]->s_vec, sp[4]->s_vec, sp[5]->s_vec, sp[0]->s_n);
    
    // initialize window
    float bufSize = sp[0]->s_n;
    interpFiltFFT_obj->window = ( t_float * )malloc(sizeof(t_float)*bufSize);
    make_window(interpFiltFFT_obj->window, bufSize);
}

// function to print to the console window when an incorrect message has been given
void messageUsage(void)
{
    post("please enter a message with the form 'set timesignal on' or 'set timesignal off'");
}

// inlet with a set message
void interpFiltFFT_set(t_interpFiltFFT *interpFiltFFT_obj, t_symbol *selector, int argc, t_atom *argv)
{
    const char *timesignal_text = "timesignal";
    const char *off = "off";
    const char *on = "on";
    
    // error check
    if(argc != 2 && argv[0].a_type != A_SYMBOL && argv[1].a_type != A_SYMBOL && strcmp(argv[0].a_w.w_symbol->s_name, timesignal_text))
        messageUsage( );
    
    if(strcmp(argv[1].a_w.w_symbol->s_name, off))
        interpFiltFFT_obj->useTimeSig = 0;
    else if(strcmp(argv[1].a_w.w_symbol->s_name, on))
        interpFiltFFT_obj->useTimeSig = 1;
    else
        messageUsage( );
    
}

// right inlet with a float to modify the step (percentage of interpolation)
void interpFiltFFT_step(t_interpFiltFFT *interpFiltFFT_obj, t_floatarg f)
{
    interpFiltFFT_obj->step = f;
}

// new function that is called everytime we create a new interpFilt object in pd
static void *interpFiltFFT_new(void)
{
    // create object from class
    t_interpFiltFFT *interpFiltFFT_obj = (t_interpFiltFFT *)pd_new(interpFiltFFT_class);
    // default signal inlet for time-domain input signal
    // create a signal inlet for filter 1
    inlet_new(&interpFiltFFT_obj->x_obj, &interpFiltFFT_obj->x_obj.ob_pd, &s_signal, &s_signal);
    // create another signal inlet for filter 2
    inlet_new(&interpFiltFFT_obj->x_obj, &interpFiltFFT_obj->x_obj.ob_pd, &s_signal, &s_signal);
    // create another signal inlet for time-domain sidechain signal
    inlet_new(&interpFiltFFT_obj->x_obj, &interpFiltFFT_obj->x_obj.ob_pd, &s_signal, &s_signal);
    // create a new inlet for set message
    inlet_new(&interpFiltFFT_obj->x_obj, &interpFiltFFT_obj->x_obj.ob_pd, gensym("set"), gensym("set"));
    // create a new inlet for float
    inlet_new(&interpFiltFFT_obj->x_obj, &interpFiltFFT_obj->x_obj.ob_pd, gensym("float"), gensym("ft_step"));
    // create a signal outlet for the filtered input signal
    outlet_new(&interpFiltFFT_obj->x_obj, gensym("signal"));
    // create a signal outlet for the interpolated filter spectrum
    outlet_new(&interpFiltFFT_obj->x_obj, gensym("signal"));
    
    return (interpFiltFFT_obj);
}

// function to clear the allocated square table
static void interpFiltFFT_free(t_interpFiltFFT *x)
{
    if(x->window != 0)
    {
        free(x->window);
        x->window = 0;
    }
}

// called when Pd is first loaded and tells Pd how to load the class
void interpFiltFFT_tilde_setup(void)
{
    interpFiltFFT_class = class_new(gensym("interpFiltFFT~"), (t_newmethod)interpFiltFFT_new, 0,
                                    sizeof(t_interpFiltFFT), 0, A_DEFFLOAT, 0);
    
    // magic to declare the leftmost inlet the main inlet that will take a signal
    // installs step as the leftmost inlet float
    CLASS_MAINSIGNALIN(interpFiltFFT_class, t_interpFiltFFT, step);
    
    // register method that will be called when dsp is turned on
    class_addmethod(interpFiltFFT_class, (t_method)interpFiltFFT_dsp, gensym("dsp"), (t_atomtype)0);
    
    // register callback method for inlet set message
    class_addmethod(interpFiltFFT_class, (t_method)interpFiltFFT_set, gensym("set"), A_GIMME, 0);
    
    // register callback method for right inlet float
    class_addmethod(interpFiltFFT_class, (t_method)interpFiltFFT_step, gensym("ft_step"), A_FLOAT, 0);
}

// ===========
// FFT METHODS
// ===========
//-----------------------------------------------------------------------------
// name: make_window()
// desc: make a hanning window
//-----------------------------------------------------------------------------
void make_window( float * window, unsigned long length )
{
    unsigned long i;
    double pi, phase = 0, delta;
    
    pi = 4.*atan(1.0);
    delta = 2 * pi / (double) length;
    
    for( i = 0; i < length; i++ )
    {
        window[i] = (float)(0.5 * (1.0 - cos(phase)));
        phase += delta;
    }
}




//-----------------------------------------------------------------------------
// name: apply_window()
// desc: apply a window to data
//-----------------------------------------------------------------------------
void apply_window( float * data, float * window, unsigned long length )
{
    unsigned long i;
    
    for( i = 0; i < length; i++ )
        data[i] *= window[i];
}

static float PI ;
static float TWOPI ;
void bit_reverse( float * x, long N );

//-----------------------------------------------------------------------------
// name: rfft()
// desc: real value fft
//
//   these routines from the CARL software, spect.c
//   check out the CARL CMusic distribution for more source code
//
//   if forward is true, rfft replaces 2*N real data points in x with N complex
//   values representing the positive frequency half of their Fourier spectrum,
//   with x[1] replaced with the real part of the Nyquist frequency value.
//
//   if forward is false, rfft expects x to contain a positive frequency
//   spectrum arranged as before, and replaces it with 2*N real values.
//
//   N MUST be a power of 2.
//
//-----------------------------------------------------------------------------
void rfft( float * x, long N, unsigned int forward )
{
    static int first = 1 ;
    float c1, c2, h1r, h1i, h2r, h2i, wr, wi, wpr, wpi, temp, theta ;
    float xr, xi ;
    long i, i1, i2, i3, i4, N2p1 ;
    
    if( first )
    {
        PI = (float) (4.*atan( 1. )) ;
        TWOPI = (float) (8.*atan( 1. )) ;
        first = 0 ;
    }
    
    theta = PI/N ;
    wr = 1. ;
    wi = 0. ;
    c1 = 0.5 ;
    
    if( forward )
    {
        c2 = -0.5 ;
        complexfft( x, N, forward ) ;
        xr = x[0] ;
        xi = x[1] ;
    }
    else
    {
        c2 = 0.5 ;
        theta = -theta ;
        xr = x[1] ;
        xi = 0. ;
        x[1] = 0. ;
    }
    
    wpr = (float) (-2.*pow( sin( 0.5*theta ), 2. )) ;
    wpi = (float) sin( theta ) ;
    N2p1 = (N<<1) + 1 ;
    
    for( i = 0 ; i <= N>>1 ; i++ )
    {
        i1 = i<<1 ;
        i2 = i1 + 1 ;
        i3 = N2p1 - i2 ;
        i4 = i3 + 1 ;
        if( i == 0 )
        {
            h1r =  c1*(x[i1] + xr ) ;
            h1i =  c1*(x[i2] - xi ) ;
            h2r = -c2*(x[i2] + xi ) ;
            h2i =  c2*(x[i1] - xr ) ;
            x[i1] =  h1r + wr*h2r - wi*h2i ;
            x[i2] =  h1i + wr*h2i + wi*h2r ;
            xr =  h1r - wr*h2r + wi*h2i ;
            xi = -h1i + wr*h2i + wi*h2r ;
        }
        else
        {
            h1r =  c1*(x[i1] + x[i3] ) ;
            h1i =  c1*(x[i2] - x[i4] ) ;
            h2r = -c2*(x[i2] + x[i4] ) ;
            h2i =  c2*(x[i1] - x[i3] ) ;
            x[i1] =  h1r + wr*h2r - wi*h2i ;
            x[i2] =  h1i + wr*h2i + wi*h2r ;
            x[i3] =  h1r - wr*h2r + wi*h2i ;
            x[i4] = -h1i + wr*h2i + wi*h2r ;
        }
        
        wr = (temp = wr)*wpr - wi*wpi + wr ;
        wi = wi*wpr + temp*wpi + wi ;
    }
    
    if( forward )
        x[1] = xr ;
    else
        complexfft( x, N, forward ) ;
}




//-----------------------------------------------------------------------------
// name: cfft()
// desc: complex value fft
//
//   these routines from CARL software, spect.c
//   check out the CARL CMusic distribution for more software
//
//   cfft replaces float array x containing NC complex values (2*NC float
//   values alternating real, imagininary, etc.) by its Fourier transform
//   if forward is true, or by its inverse Fourier transform ifforward is
//   false, using a recursive Fast Fourier transform method due to
//   Danielson and Lanczos.
//
//   NC MUST be a power of 2.
//
//-----------------------------------------------------------------------------
void complexfft( float * x, long NC, unsigned int forward )
{
    float wr, wi, wpr, wpi, theta, scale ;
    long mmax, ND, m, i, j, delta ;
    ND = NC<<1 ;
    bit_reverse( x, ND ) ;
    
    for( mmax = 2 ; mmax < ND ; mmax = delta )
    {
        delta = mmax<<1 ;
        theta = TWOPI/( forward? mmax : -mmax ) ;
        wpr = (float) (-2.*pow( sin( 0.5*theta ), 2. )) ;
        wpi = (float) sin( theta ) ;
        wr = 1. ;
        wi = 0. ;
        
        for( m = 0 ; m < mmax ; m += 2 )
        {
            register float rtemp, itemp ;
            for( i = m ; i < ND ; i += delta )
            {
                j = i + mmax ;
                rtemp = wr*x[j] - wi*x[j+1] ;
                itemp = wr*x[j+1] + wi*x[j] ;
                x[j] = x[i] - rtemp ;
                x[j+1] = x[i+1] - itemp ;
                x[i] += rtemp ;
                x[i+1] += itemp ;
            }
            
            wr = (rtemp = wr)*wpr - wi*wpi + wr ;
            wi = wi*wpr + rtemp*wpi + wi ;
        }
    }
    
    // scale output
    scale = (float)(forward ? 1./ND : 2.) ;
    {
        register float *xi=x, *xe=x+ND ;
        while( xi < xe )
            *xi++ *= scale ;
    }
}




//-----------------------------------------------------------------------------
// name: bit_reverse()
// desc: bitreverse places float array x containing N/2 complex values
//       into bit-reversed order
//-----------------------------------------------------------------------------
void bit_reverse( float * x, long N )
{
    float rtemp, itemp ;
    long i, j, m ;
    for( i = j = 0 ; i < N ; i += 2, j += m )
    {
        if( j > i )
        {
            rtemp = x[j] ; itemp = x[j+1] ; /* complex exchange */
            x[j] = x[i] ; x[j+1] = x[i+1] ;
            x[i] = rtemp ; x[i+1] = itemp ;
        }
        
        for( m = N>>1 ; m >= 2 && j >= m ; m >>= 1 )
            j -= m ;
    }
}
